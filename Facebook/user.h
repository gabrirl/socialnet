#ifndef USER_H
#define USER_H
#include <QString>
#include <QSqlField>
#include <QSqlRecord>
#include "database.h"



class User : public QObject
{
    Q_OBJECT
private:
    int id;
    QString pseudo;
    Database* db;

signals:
    void new_post(int, int);

public:
    User(Database* d,int, QString);
    User(Database* d, int id);

    QString getPseudo() const;
    void newPost(QString);


    friend bool operator <(const User u1, const User u2){
        return u1.id < u2.id;
    }
};

#endif // USER_H
