#include "userlist.h"

UserList::UserList(QWidget *parent, Database* d) : QWidget(parent)
{
    /*
     * Classe permettant d'afficher la liste des utilisateur, de faire une recherche
     * sur les utilisateurs(avec leur pseudo) et de créer un nouvelle utilisateur
     */

    db = d;

    tableView = new QTableView();
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(tableView,SIGNAL(clicked(QModelIndex)),this,SLOT(display_in_view(QModelIndex)));
    tableView->setAlternatingRowColors(true);


    search_bar = new QLineEdit();
    search_bar->setPlaceholderText("Chercher");
    connect(search_bar,SIGNAL(textEdited(QString)),SLOT(search_by_name(QString)));
   /* QCompleter *completer = new QCompleter();
    search_bar->setCompleter(completer);*/

    head_message = new QLabel(tr("Users"));
    head_message->setStyleSheet("font-size: 30px");

    btn_add = new QPushButton("New User");
    connect(btn_add, SIGNAL(clicked(bool)), this, SLOT(addUser()));

    layout = new QGridLayout(this);

    layout->addWidget(search_bar,1,0,1,1);
    layout->addWidget(btn_add,1,1,1,1);

    layout->addWidget(head_message,0,0,1,9);
    layout->addWidget(tableView,2,0,1,9);

    setLayout(layout);

    update_view();

}

void UserList::update_view()
{
    QSqlQueryModel* model = db->getUsers();

    tableView->setModel(model);
    tableView->hideColumn(0);
}

void UserList::addUser()
{
    NewUserForm* f = new NewUserForm(db, this);
    connect(f, SIGNAL(newUser()), this, SLOT(update_view()));
    f->show();
}

void UserList::search_by_name(QString str)
{
    if(str.isEmpty()){
        update_view();
    }
    else{
        QSqlQueryModel* model = db->getUsersByPseudo(str);

        tableView->setModel(model);
    }
}

void UserList::display_in_view(QModelIndex m){


    QItemSelectionModel *selection = tableView->selectionModel();
    QModelIndexList index_elements = selection->selectedIndexes();

    emit(userClicked(index_elements));


}
