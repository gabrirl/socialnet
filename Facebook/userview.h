#ifndef USERVIEW_H
#define USERVIEW_H

#include <QWidget>
#include <QLabel>
#include <QTabWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QTableView>
#include <QHeaderView>
#include <QMessageBox>
#include "database.h"
#include "userlist.h"
#include "postform.h"
#include "showpost.h"

class UserView : public QWidget
{
    Q_OBJECT

public:
    explicit UserView(Database*, QWidget*);
    int getUser() const;

public slots:
    void updateView(QModelIndexList);
    void updateInbox();
    void updateFriends();
    void updateSent();
    void showPost(QModelIndex);
    void showMyPost(QModelIndex);

    void addRelation();
    void newRelation();
    void newPost();
    void deleteUser();
    void deleteRelation();
    void enableDeleteRelation(QModelIndex);


signals:
    void supressed();
private:
    Database* db;
    int uid;

    UserList* ulist;

    QTableView* table_friends;
    QTableView* table_inbox;
    QTableView* table_sent;

    QGridLayout* grid;
    QLabel* pseudo;
    QTabWidget* tab;
    QPushButton* btn_newPost;
    QPushButton* btn_delete;
    QPushButton* btn_newRelation;
    QPushButton* btn_deleteRelation;


};

#endif // USERVIEW_H
