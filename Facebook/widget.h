#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "database.h"
#include "graph.h"
#include "userlist.h"
#include "userview.h"
#include "postform.h"

class Widget : public QWidget
{
    Q_OBJECT

private:
    Graph* graph;
    Database* db;

    UserView* uview;
    UserList* ulist;
    QGridLayout* grid;

public:
    Widget(QWidget *parent = 0);
    void newPost();
//    deletePost();
//    deleteUser();
//    newRelation();
//    deleteRelation();

    ~Widget();
};

#endif // WIDGET_H
