#include "newuserform.h"

NewUserForm::NewUserForm(Database* d, QWidget* parent): QDialog(parent)
{

    /*
     * Classe fournissant un formulaire pour
     * la création d'un nouvelle utilisateur
     */
    db = d;

    btn_ok = new QPushButton("OK");
    connect(btn_ok, SIGNAL(clicked(bool)), this, SLOT(addUser()));

    btn_cancel = new QPushButton("Annulé");
    connect(btn_cancel, SIGNAL(clicked(bool)), this, SLOT(close()));

    pseudo = new QLineEdit(this);

    layout = new QFormLayout(this);
    layout->addRow("Pseudo", pseudo);
    layout->addRow(btn_cancel);
    layout->addRow(btn_ok);

    setModal(true);
    setLayout(layout);

}

void NewUserForm::addUser()
{

    if(!pseudo->text().isEmpty()){
         db->addUser(pseudo->text());
         emit(newUser());
         close();
    }else{
        QMessageBox::critical(this,"Erreur","Veuillez rentrer un pseudo");
    }




}
