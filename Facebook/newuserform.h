#ifndef NEWUSERFORM_H
#define NEWUSERFORM_H

#include <QDialog>
#include <QPushButton>
#include <QFormLayout>
#include <QMessageBox>
#include <QLineEdit>
#include "database.h"

class NewUserForm : public QDialog
{
    Q_OBJECT

public:
    NewUserForm(Database*, QWidget*);

signals:
    void newUser();

public slots:
    void addUser();

private:
    Database* db;
    QLineEdit* pseudo;
    QPushButton* btn_ok;
    QPushButton* btn_cancel;
    QFormLayout* layout;

};

#endif // NEWUSERFORM_H
