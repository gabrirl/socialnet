#ifndef USERLIST_H
#define USERLIST_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QLineEdit>
#include "database.h"
#include "newuserform.h"

class UserList : public QWidget
{
    Q_OBJECT

public:
    explicit UserList(QWidget*, Database*);
    QTableView *tableView;

signals:
    void userClicked(QModelIndexList);

public slots:

    void display_in_view(QModelIndex);
    void search_by_name(QString str);
    void update_view();
    void addUser();

private:


    QString table;

    QLabel *head_message;

    Database* db;


    //Recherche et tri
    QPushButton* btn_add;
    QLineEdit* search_bar;
    QGridLayout* layout;
};

#endif // USERLIST_H
