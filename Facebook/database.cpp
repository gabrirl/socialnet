#include "database.h"


Database::Database(QString n, QObject *parent) : QObject(parent)
{
    /* Constructeur de la classe Database
     * initilisant une connexion à une base de
     * donnée SQLITE de nom 'n'.
     *
     * Parametres: nom de la base de donnée
     * Retourne: Rien
     */

    conn = QSqlDatabase::addDatabase("QSQLITE");
    conn.setDatabaseName(n);

    if(!conn.open()){
        /* On quitte le programme en cas d'échec de
         * l'ouverture de la base de donnée
         * avec le code 1
         */

        qDebug()<<conn.lastError().text();

        exit(1);
    }

}

QSqlQueryModel* Database::getRelations()
{
    /* Fonction chargée de la récupération
     * des relations d'amitiée entre les utilisateurs
     * de la base de donnée.
     *
     * Parametres: Rien
     * Retourne: la liste des relations de
     *           la base de donnée sous forme
     *           d'objet QSqlQueryModel
     */

    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    if(!q->exec("select u1.id, u1.pseudo, u2.id, u2.pseudo "
                "from User u1, Relation "
                "inner join User u2 "
                "where Relation.usr1 = u1.id "
                "and Relation.usr2 = u2.id ")){

        /* Affichage de l'erreur en console en
         * cas d'échec
         */

        qDebug()<<q->lastError().text();
    }
    model->setQuery(*q);

    return model;
}

int Database::addPost(QString c, int u)
{

    /* Fonction permettant d'ajouter un nouveau poste
     *
     * Parametres: Contenu c du poste et id u de l'utilisateur
     *            éffectuant le post
     *
     * Retourne: l'id du poste nouvellement crée.
     */

    int rtn;

    QSqlQuery* q = new QSqlQuery(conn);

    //Ajout du poste
    q->prepare("insert into Publication(user, contenu, date_pub) "
               "values(?,?,?)");
    q->bindValue(0, u);
    q->bindValue(1, c);
    q->bindValue(2, QDateTime::currentDateTime().toString("dd:MM:yyyy hh:mm:ss"));

    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }


   q->clear();

   /* Récupération de l'id du dernier poste ajouté
    * à la base de donnée (le poste qui vient d'être ajouté)
    */

    if(!q->exec("select id from Publication "
                " Order by date_pub desc limit 0,1")){
        qDebug()<<q->lastError().text();
    }

    while(q->next()){

        rtn = q->value(0).toInt();
    }

    return rtn;






}

void Database::addRelation(int u1, int u2)
{
    /*
     * Méthode pour ajouter une relation
     * d'amitier entre deux utilisateurs distincts.
     * Parametres : ids u1 et u2 des deux utilisateurs
     * Retourne : Rien
     */

    if(u1 != u2){
        QSqlQuery* q = new QSqlQuery(conn);
        q->prepare("insert into Relation(usr1, usr2) "
                   "values(?,?)");
        q->bindValue(0, u1);
        q->bindValue(1, u2);

        if(!q->exec()){
            qDebug()<<q->lastError().text();
        }
    }

}

void Database::addReception(int u, int p)
{
    /* Fonction permettant d'enregistrer les réception
     * des postes par les utilisateurs.
     *
     * Parametres: id p du poste, id u de l'utilisateur
     *             récipiendaire
     * Retourne: Rien
     */

    QSqlQuery* q = new QSqlQuery(conn);
    q->prepare("insert into Reception(user, publication, date_rec) "
               "values(?,?,?)");
    q->bindValue(0, u);
    q->bindValue(1, p);
    q->bindValue(2, QDateTime::currentDateTime().toString("dd:MM:yyyy hh:mm:ss"));

    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }
}

void Database::addUser(QString ps)
{
    /*
     * Méthode d'ajout d'un utilisateur
     * Parametre : Pseudo ps de l'utilsateur
     * Retourne : rien
     */

    QSqlQuery* q = new QSqlQuery(conn);
    q->prepare("insert into User(pseudo) "
               "values(?)");
    q->bindValue(0, ps);

    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }

}

void Database::deleteUser(int id)
{
    /*
     * Méthode de suppression d'un utilisateur
     * Parametre : id de l'utilisateur
     * Retourne : Rien
     */

    QSqlQuery* q = new QSqlQuery(conn);
    q->prepare("delete from User "
               "where id = ?");
    q->bindValue(0, id);

    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }

    /*
     * On Supprime ensuite tous les postes
     * et toutes les relations d'amitiées liée
     * à ce dernier
     */

    deletePostsOf(id);
    deleteRelationsOf(id);

}

void Database::deleteRelationsOf(int id)
{
    /*
     * Méthode pour la suppression de tous
     * les relations dans lesquelles apparait un
     * utilisateur donné.
     *
     * Parametre : id de l'utilisateur
     * Retourne : rien
     */
    QSqlQuery* q = new QSqlQuery(conn);
    q->prepare("delete from Relation "
               "where usr1 = ? or usr2 = ?");
    q->bindValue(0, id);
    q->bindValue(0, id);

    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }


}

void Database::deletePostsOf(int id)
{
    /*
     * Méthode de suppression des publications
     * d'un utilisateur donnée.
     *
     * Parametre : id de l'utilisateur
     * Retourne : Rien
     */

    /*
     * On supprime d'abord toutes les réceptions liées
     * à la publication
     */

    QSqlQuery* q = new QSqlQuery(conn);
    q->prepare("delete from Reception "
               "where publication in (select id from Publication where user = ?");
    q->bindValue(0, id);


    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }

    /*
     * Puis on supprime la publication proprement dite
     */

    q->prepare("delete from Publication "
               "where user = ?");
    q->bindValue(0, id);


    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }
}

void Database::deleteRelation(int id1, int id2)
{
    /*
     * Méthode de suppression d'une relation entre deux
     * utilisateurs
     *
     * Parametre : id des deux utilisateurs (id1 et id2)
     * Retourne : Rien
     */
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("delete from Relation "
               "where (usr1 = ? and usr2 = ?) or (usr2 = ? and usr1 = ?) ");
    q->bindValue(0, id1);
    q->bindValue(1, id2);
    q->bindValue(2, id2);
    q->bindValue(3, id1);


    if(!q->exec()){
        qDebug()<<q->lastError().text();
    }
}

QSqlQueryModel *Database::getFriendsOf(int id)
{
    /*
     * Méthode pour obtenir la liste de tous les utilisateurs
     * amis à un utilisateur ayant pour identifiant 'id'
     *
     * Parametre : id de l'utilisateur
     * Retourne : liste de tous ses amis sous forme de
     *             pointeur d'objet QSqlQueryModel
     */

    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("select u1.id, u1.pseudo from Relation, User u1 "
               " where usr2 = ? and usr1 = u1.id "
               " union "
               " select u2.id, u2.pseudo from Relation, User u2 "
               " where usr1 = ? and usr2 = u2.id ");
    q->bindValue(0, id);
    q->bindValue(1, id);

    if(!q->exec()){

        qDebug()<<q->lastError().text();
    }

    model->setQuery(*q);

    return model;

}


QSqlQueryModel *Database::getInboxOf(int id)
{
    /*
     * Méthode pour obtenir la liste de tous les postes
     * ayant parvenu à l'utilisateur d'identifiant 'id'
     *
     * Parametre : id de l'utilisateur
     * Retourne : liste de toutes les publications
     *           parvenus à l'utilisateur sous forme de
     *           pointeur d'objet QSqlQueryModel
     */

    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("select Publication.id, Publication.contenu as Poste, Publication.date_pub as Date, u2.pseudo "
               " from User u1, Reception "
               " join User u2, Publication "
               " Where Reception.user = u1.id and Reception.user = ? "
               "     and Publication.id = Reception.publication and Publication.user = u2.id order by Publication.date_pub");
    q->bindValue(0, id);

    if(!q->exec()){

        qDebug()<<q->lastError().text();
    }

    model->setQuery(*q);

    return model;

}

QSqlQueryModel *Database::getSentOf(int id)
{

    /*
     * Méthode pour obtenir la liste de tous les postes
     * par l'utilisateur d'identifiant 'id'
     *
     * Parametre : id de l'utilisateur
     * Retourne : liste de toutes les publications
     *           parvenus à l'utilisateur sous forme de
     *           pointeur d'objet QSqlQueryModel
     */
    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("select Publication.id, Publication.contenu as Poste, Publication.date_pub as Date "
               "from Publication, User "
               "Where Publication.user = User.id and User = ? order by Publication.date_pub");
    q->bindValue(0, id);


    if(!q->exec()){

        qDebug()<<q->lastError().text();
    }

    model->setQuery(*q);

    return model;

}

QSqlQueryModel *Database::getUsers()
{
    /*
     * Méthode pour obtenir la liste de tous les utilisateurs
     * de la base de donnée
     *
     * Parametre : Rien
     * Retourne : liste de tous les utilisateurs sous forme de
     *             pointeur d'objet QSqlQueryModel
     */

    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("select id, pseudo from User order by pseudo");

    if(!q->exec()){

        qDebug()<<q->lastError().text();
    }

    model->setQuery(*q);

    return model;
}

QSqlQueryModel *Database::getUsersByPseudo(QString str)
{
    /*
     * Méthode pour obtenir la liste de tous les utilisateurs
     * de la base de donnée ayant un pseudo commençant par
     * la valeur de la variable 'str'
     * Parametre : Variable QString str
     * Retourne : liste de tous les utilisateurs de pseudo
     *            commançant par la valeur de 'str' sous forme de
     *            pointeur d'objet QSqlQueryModel
     */
    QSqlQueryModel* model = new QSqlQueryModel(nullptr);
    QSqlQuery* q = new QSqlQuery(conn);

    q->prepare("select id, pseudo from User where pseudo like ? order by pseudo");
    q->bindValue(0, str+"%");
    if(!q->exec()){

        qDebug()<<q->lastError().text();
    }

    model->setQuery(*q);

    return model;
}
