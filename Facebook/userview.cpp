#include "userview.h"

UserView::UserView(Database* d, QWidget* parent): QWidget(parent)
{
    /*
     * Classe permettant d'afficher les informations sur un utilisateur
     */

      db = d;

      btn_newPost = new QPushButton("Nouvelle publication", this);
      connect(btn_newPost, SIGNAL(clicked(bool)), this, SLOT(newPost()));

      btn_delete = new QPushButton("Supprimer le compte", this);
      connect(btn_delete, SIGNAL(clicked(bool)), this, SLOT(deleteUser()));

      btn_newRelation = new QPushButton("Envoyer une demande d'amie ", this);
      connect(btn_newRelation, SIGNAL(clicked(bool)), this, SLOT(addRelation()));

      btn_deleteRelation = new QPushButton("Supprimer de mes amis", this);
      btn_deleteRelation->setDisabled(true);
      connect(btn_deleteRelation, SIGNAL(clicked(bool)), this, SLOT(deleteRelation()));

      tab = new QTabWidget(this);

      table_friends = new QTableView(this);
      table_inbox = new QTableView(this);
      table_sent = new QTableView(this);


      table_friends->horizontalHeader()->setStretchLastSection(true);
      table_friends->setSelectionBehavior(QAbstractItemView::SelectRows);
      table_friends->setSelectionMode(QAbstractItemView::SingleSelection);
      table_friends->setAlternatingRowColors(true);
      connect(table_friends, SIGNAL(clicked(QModelIndex)), this, SLOT(enableDeleteRelation(QModelIndex)));

      table_inbox->horizontalHeader()->setStretchLastSection(true);
      table_inbox->setSelectionBehavior(QAbstractItemView::SelectRows);
      table_inbox->setSelectionMode(QAbstractItemView::SingleSelection);
      table_inbox->setAlternatingRowColors(true);
      connect(table_inbox, SIGNAL(clicked(QModelIndex)), this, SLOT(showPost(QModelIndex)));

      table_sent->horizontalHeader()->setStretchLastSection(true);
      table_sent->setSelectionBehavior(QAbstractItemView::SelectRows);
      table_sent->setSelectionMode(QAbstractItemView::SingleSelection);
      table_sent->setAlternatingRowColors(true);
      connect(table_sent, SIGNAL(clicked(QModelIndex)), this, SLOT(showMyPost(QModelIndex)));


      tab->addTab(table_friends, "Mes amis");
      tab->addTab(table_sent, "Mes postes");
      tab->addTab(table_inbox, "Boite de reception");

      pseudo = new QLabel("pseudo", this);
      pseudo->setStyleSheet("font-size:30px; color: blue");

      grid = new QGridLayout(this);
      grid->addWidget(pseudo,0,0,1,2);
      grid->addWidget(btn_newPost,1,0,1,1);
      grid->addWidget(btn_delete,1,1,1,1);
      grid->addWidget(btn_newRelation,1,2,1,1);
      grid->addWidget(btn_deleteRelation,1,3,1,1);
      grid->addWidget(tab,2,0,4,4);

      setLayout(grid);

}

void UserView::updateFriends(){

    QSqlQueryModel* model = db->getFriendsOf(uid);
    btn_deleteRelation->setDisabled(true);
    table_friends->setModel(model);
    table_friends->hideColumn(0);

}

void UserView::updateInbox(){

    QSqlQueryModel* model = db->getInboxOf(uid);
    table_inbox->setModel(model);
    table_inbox->hideColumn(0);

}

void UserView::updateSent(){

    QSqlQueryModel* model = db->getSentOf(uid);
    table_sent->setModel(model);
    table_sent->hideColumn(0);

}

void UserView::showPost(QModelIndex)
{
    QItemSelectionModel *selection = table_inbox->selectionModel();
    QModelIndexList index_elements = selection->selectedIndexes();


    ShowPost* p = new ShowPost(this, index_elements, false);
    p->show();
}

void UserView::showMyPost(QModelIndex)
{
    QItemSelectionModel *selection = table_sent->selectionModel();
    QModelIndexList index_elements = selection->selectedIndexes();


    ShowPost* p = new ShowPost(this, index_elements, true);
    p->show();
}

void UserView::addRelation()
{
    QDialog* dialog = new QDialog(this);
    QFormLayout* fl = new QFormLayout(dialog);
    ulist = new UserList(dialog, db);
    ulist->tableView->setSelectionMode(QAbstractItemView::MultiSelection);

    QPushButton* btn_ok = new QPushButton(tr("Ajouter"));
    connect(btn_ok,SIGNAL(clicked(bool)),this,SLOT(newRelation()));


    fl->addWidget(ulist);
    fl->addWidget(btn_ok);


    fl->addWidget(ulist);
    fl->addWidget(btn_ok);

    dialog->setLayout(fl);
    dialog->setFixedWidth(600);
    dialog->setFixedHeight(350);
    dialog->setWindowTitle(tr("Nouvelle amitié"));
    dialog->setWindowModality(Qt::WindowModal);


    dialog->setLayout(fl);
    dialog->show();

}

void UserView::newRelation()
{
    QItemSelectionModel *selection = ulist->tableView->selectionModel();
    QModelIndexList index_elements = selection->selectedIndexes();


    for(int i = 0; i < index_elements.size(); i+=2){
       db->addRelation(uid, index_elements.at(i).data(0).toInt());
    }

    updateFriends();

}

void UserView::newPost()
{
    PostForm* f = new PostForm(db, uid, NULL);
    connect(f, SIGNAL(ended()), this, SLOT(updateSent()));
    f->show();
}

void UserView::deleteUser()
{
    QMessageBox msgBox;
    msgBox.setFixedWidth(900);
    msgBox.setText(tr("Suppression"));
    msgBox.setInformativeText("Voulez vous vraiment supprimer ce utilisateur ?" );
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::No:
        msgBox.close();
        break;
    case QMessageBox::Yes:

        db->deleteUser(uid);
        this->setDisabled(true);
        emit(supressed());

        break;

    }

}

void UserView::deleteRelation()
{
    QMessageBox msgBox;
    msgBox.setFixedWidth(900);
    msgBox.setText(tr("Suppression"));
    msgBox.setInformativeText("Voulez vous vraiment supprimer cette relation ?" );
    msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::No:
        msgBox.close();
        break;
    case QMessageBox::Yes:

        QItemSelectionModel *selection = table_friends->selectionModel();
        QModelIndexList index_elements = selection->selectedIndexes();


        for(int i = 0; i < index_elements.size(); i+=2){
           db->deleteRelation(uid, index_elements.at(i).data(0).toInt());
        }

        updateFriends();

        break;

    }
}

void UserView::enableDeleteRelation(QModelIndex)
{
    btn_deleteRelation->setEnabled(true);
}

int UserView::getUser() const
{
    return uid;
}

void UserView::updateView(QModelIndexList m)
{
    uid = m.at(0).data(0).toInt();
    pseudo->setText(m.at(1).data(0).toString());
    updateFriends();
    updateSent();
    updateInbox();


    this->setDisabled(false);


}


