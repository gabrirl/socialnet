#ifndef DATABASE_H
#define DATABASE_H

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QDebug>
#include <QDateTime>
#include <QObject>


class Database: public QObject
{

    /**
     * Classe database permettant la connexion
     * et la gestion des requêtes sur
     *  la base de donnée
     */

private:
    QString name;
    QSqlDatabase conn;

public:
   // Database(QObject*);
    Database(QString, QObject* parent = nullptr);
    QSqlQueryModel* getRelations();
    int addPost(QString, int);
    void addReception(int, int);
    void addRelation(int, int);
    void addUser(QString);
    void deleteUser(int);
    void deleteRelationsOf(int);
    void deletePostsOf(int);
    void deleteRelation(int, int);

    QSqlQueryModel* getFriendsOf(int);
    QSqlQueryModel* getInboxOf(int);
    QSqlQueryModel* getSentOf(int);
    QSqlQueryModel* getUsers();
    QSqlQueryModel* getUsersByPseudo(QString);

};

#endif // DATABASE_H
