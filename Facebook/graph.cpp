#include "graph.h"

/* Description de la structure de donnée
 * (E,V)
 *
 * E = liste des ids des utilisateurs
 * V = (u,v) avec u et v élements de E
 *
 * Un sommet est ici l'id d'un utilisateur et
 * il existe une arete en un sommet u et et sommet v
 * si les utilisateurs u et v sont amis.
 *
 * On modélise ici un graphe en associant à chaque
 * sommet la liste de ses sommets adjacents.
 *
 * La liste des sommets adjencents à un autre sommet
 * ne doit pas contenir des doublons. On utilisera donc
 * pour contenir la liste des sommets un set.
 * La recherche d’un élément a une complexité O(log(n)).
 *
 * Pour garder la liste des sommets, on utilise un map, ayant une bonne
 * complexité en matiere d'acces O(1). Ce qui nous sera utilise pour
 * l'implementation du BFS.
 *
 *
 * Exemple:
 *
 * 1-------------------2
 * |
 * 3-------------------4
 * |                  /\
 * |                 /  \
 * |                /    \
 * 5               /      \
 *                6        7
 *
 *
 * En memoire:
 *
 * data[1] = [1, 2]
 * data[2] = [1]
 * data[3] = [1, 4, 5]
 * data[4] = [3, 6, 7]
 * data[5] = [3]
 * data[6] = [4]
 * data[7] = [7]
 *
 *
 */

Database *Graph::getDb() const
{
    return db;
}

void Graph::setDb(Database *value)
{
    db = value;
}

Graph::Graph(QObject *parent = nullptr) : QObject(parent)
{
    /* Constructeur de la classe Graph
     * permettant l'initialisation de la
     * base de donnée
     */

}

void Graph::populate()
{
    /* Méthode permettant de construire la structure
     * du Graphe à partir des informations de la base de donnée
     *
     * Parametres: Rien
     * Retourne: Rien
     */

    /* On recupère la liste des relations de la base de donnée
     * grâce à la méthode getRelations() de la classe Database
     */

    QSqlQueryModel* model = db->getRelations();

    /* Pour chaque ligne de la table,
     * on ajoute l'id de l'utilisateur usr1 à la
     * liste des amis de l'utilisateur usr2 et vis
     * versa.
     *
     */

    for(int i = 0; i < model-> rowCount(QModelIndex()); i++){

        int usr1 = model->record(i).field(0).value().toInt();
        int usr2 = model->record(i).field(2).value().toInt();
        if(usr1 != usr2){
            data[usr1].insert(usr2);
            data[usr2].insert(usr1);
        }

    }
}

void Graph::show()
{

    /* Méthode permettant d'afficher le graphe en console
     * Parametres: Rien
     * Retourne; Rien
     */

    for (QMap<int ,std::set<int> >::const_iterator p=data.begin();
    p != data.end();
         ++p){

         qDebug()<< "User " << p.key();
        for(std::set<int>::const_iterator s=p.value().begin(); s != p.value().end();
            ++s){
            qDebug()<< " friends " << (*s)  ;
        }
        qDebug()<< "";

    }

}

void Graph::sendPost(int u, int p)
{
    /*
     * Méthode permettant l'envoie
     * d'un poste d'identifiant p à un utilisateur d'identifiant u.
     *
     * Parametre : u et p
     * Retourne : Rien
     */

    db->addReception(u, p);
}

void Graph::BFS(int start, int post)
{

    /* Methode permettant de propager une
     * publication au sein du graphe.
     * On parcour ici le graphe en largeur en envoyant le
     * poste à chaque noeud visté du réseau.
     *
     * Parametre: id de l'utilisateur de départ
     *            id de la publication
     *
     * Retourne: Rien
     */

                /*
                 * Si la file n'est pas vide,
                 * on retire son élément de tête
                 */

                if(!qu.empty())	qu.pop();

                /*
                 * Si le sommet actuellement visité n'est pas
                 * déjà visité, on lui enverra à l'utilisateur à ce noeud,
                 * le poste en cours de propagation, puis on marque le sommet
                 * comme étant parcourue
                 */

                if(memo[start] == false){
                      sendPost(start, post);
                      memo[start] = true;
                }


                /*
                 * On ajoute à la file, chaque sommet adjascent au sommet
                 * actuellement visité qui n'ait pas encore été visité.
                 */

               for(std::set<int>::const_iterator s=data[start].begin(); s != data[start].end();
                    ++s){

                    if(memo[*s] == false){
                         qu.push(*s);
                    }

                }


                /*
                 * Tant que la file n'est pas vide,
                 * on recommance depuis le haut
                 */

                if(!qu.empty()){
                    BFS(qu.front(), post);
                }

}

void Graph::newPost(int u, int p)
{
    /*
     * Méthode pour l'ajout d'un nouveau poste.
     * On génére le graphe à partir de la base de donnée,
     * puis on marque le noeud de l'utilisateur faisant la publication,
     * comme étant déjà visité(pour éviter qu'il ne reçoive son propre poste).
     * On propage ensuite le poste au sein du réseau.
     *
     * Parametre : id de l'utilisateur (u), id  du poste (p)
     * Retourne : Rien
     */

    populate();
    memo[u] = true;

    BFS(u, p);
}






