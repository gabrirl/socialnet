#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    db = new Database("db/facebook.sqlite", NULL);

        ulist = new UserList(this, db);
        uview = new UserView(db, this);

        connect(ulist, SIGNAL(userClicked(QModelIndexList)), uview, SLOT(updateView(QModelIndexList)));

        connect(uview, SIGNAL(supressed()), ulist, SLOT(update_view()));

        grid = new QGridLayout(this);
        grid->addWidget(ulist,1,0,1,1);
        grid->addWidget(uview,1,1,1,1);

        setLayout(grid);


        //this->setWindowIcon(QIcon(":/img/F.jpg"));

}

void Widget::newPost()
{
    PostForm* f = new PostForm(db, uview->getUser(),NULL);
    f->show();
}

Widget::~Widget()
{

}
