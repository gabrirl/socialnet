#include "showpost.h"

ShowPost::ShowPost(QWidget *parent, QModelIndexList m, bool mine) : QDialog(parent)
{

    /*
     * Classe fournissant permettant d'afficher un poste.
     */

    author = new QLabel();

    if(mine == true){

        author->setText("Vous");
    }else{
        author->setText(m.at(3).data(0).toString());
    }


    content = new QLabel(m.at(1).data(0).toString(), this);
    date = new QLabel(m.at(2).data(0).toString(),this);
    btn_ok = new QPushButton("OK", this);
    connect(btn_ok, SIGNAL(clicked(bool)), this, SLOT(close()));

    layout = new QFormLayout(this);
    layout->addRow("Par : ", author);
    layout->addRow("Contenu : ",content);
    layout->addRow("Envoyé le : ",date);
    layout->addRow(btn_ok);

    setFixedWidth(500);
    setLayout(layout);

}
