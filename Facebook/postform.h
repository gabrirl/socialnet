#ifndef POSTFORM_H
#define POSTFORM_H

#include <QDialog>
#include <QFormLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QMessageBox>
#include "database.h"
#include "user.h"
#include "graph.h"

class PostForm : public QDialog
{

    Q_OBJECT
private:
    QFormLayout* layout;
    QPushButton* btn_ok;
    QPushButton* btn_cancel;
    QTextEdit* txt_edit;
    int uid;
    Database* db;

public:
    PostForm(Database*,int,QDialog*);
    ~PostForm();

public slots:
    void addPost();
signals:
    void ended();
};

#endif // POSTFORM_H
