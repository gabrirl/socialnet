#ifndef GRAPH_H
#define GRAPH_H

#include <QVector>
#include <set>
#include <queue>
#include <QDebug>
#include "user.h"
#include "database.h"

class Graph: public QObject
{

    Q_OBJECT
    /* Classe Graph devant servir à
     * la modélisation du reseau social
     */

    Database* db;
public:

    /* Attribut db représentant la base de
     * à partir de laquelle opère le graphe
     */


    explicit Graph(QObject *parent);
    void populate();
    void show();



    Database *getDb() const;
    void setDb(Database *value);

public slots:
    void sendPost(int,int);
    void BFS(int, int post);
    void newPost(int,int);
private:


    QMap< int, std::set<int> > data;
    std::queue<int> qu;
    std::map<int, bool>  memo;


};

#endif // GRAPH_H
