#ifndef SHOWPOST_H
#define SHOWPOST_H

#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QModelIndexList>
#include <QPushButton>
#include <QDebug>

class ShowPost : public QDialog
{
    Q_OBJECT
private:
    QLabel* author;
    QLabel* content;
    QLabel* date;

    QFormLayout* layout;
    QPushButton* btn_ok;
public:
    explicit ShowPost(QWidget *, QModelIndexList, bool);

signals:

public slots:

};

#endif // SHOWPOST_H
