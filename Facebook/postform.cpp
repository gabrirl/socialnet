#include "postform.h"

PostForm::PostForm(Database* d, int id, QDialog *parent):QDialog(parent)
{

    /*
     * Classe fournissant un formulaire pour
     * la création d'un nouveau poste
     */

    db = d;
    uid = id;

    txt_edit = new QTextEdit(this);

    btn_ok = new QPushButton("Envoyé");
    connect(btn_ok, SIGNAL(clicked(bool)), this, SLOT(addPost()));

    btn_cancel = new QPushButton("Annulé");
    connect(btn_cancel, SIGNAL(clicked(bool)), this, SLOT(close()));

    layout = new QFormLayout(this);
    layout->addRow("Votre poste", txt_edit);
    layout->addRow(btn_cancel);
    layout->addRow(btn_ok);

    setModal(true);
    setLayout(layout);

}



void PostForm::addPost()
{
    User* u = new User(db, uid);
    Graph* g = new Graph(NULL);
    g->setDb(db);

    connect(u, SIGNAL(new_post(int,int)), g, SLOT(newPost(int,int)));

    if(!txt_edit->document()->toPlainText().isEmpty()){
        u->newPost(  txt_edit->document()->toPlainText() );
        emit(ended());
        close();

    }else{
        QMessageBox::critical(this,"Erreur","Votre publication ne peut être vide");
    }


}

PostForm::~PostForm(){

}
