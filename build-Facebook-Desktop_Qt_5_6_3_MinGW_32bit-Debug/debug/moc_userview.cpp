/****************************************************************************
** Meta object code from reading C++ file 'userview.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Facebook/userview.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'userview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_UserView_t {
    QByteArrayData data[16];
    char stringdata0[183];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UserView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UserView_t qt_meta_stringdata_UserView = {
    {
QT_MOC_LITERAL(0, 0, 8), // "UserView"
QT_MOC_LITERAL(1, 9, 9), // "supressed"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 10), // "updateView"
QT_MOC_LITERAL(4, 31, 15), // "QModelIndexList"
QT_MOC_LITERAL(5, 47, 11), // "updateInbox"
QT_MOC_LITERAL(6, 59, 13), // "updateFriends"
QT_MOC_LITERAL(7, 73, 10), // "updateSent"
QT_MOC_LITERAL(8, 84, 8), // "showPost"
QT_MOC_LITERAL(9, 93, 10), // "showMyPost"
QT_MOC_LITERAL(10, 104, 11), // "addRelation"
QT_MOC_LITERAL(11, 116, 11), // "newRelation"
QT_MOC_LITERAL(12, 128, 7), // "newPost"
QT_MOC_LITERAL(13, 136, 10), // "deleteUser"
QT_MOC_LITERAL(14, 147, 14), // "deleteRelation"
QT_MOC_LITERAL(15, 162, 20) // "enableDeleteRelation"

    },
    "UserView\0supressed\0\0updateView\0"
    "QModelIndexList\0updateInbox\0updateFriends\0"
    "updateSent\0showPost\0showMyPost\0"
    "addRelation\0newRelation\0newPost\0"
    "deleteUser\0deleteRelation\0"
    "enableDeleteRelation"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UserView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   80,    2, 0x0a /* Public */,
       5,    0,   83,    2, 0x0a /* Public */,
       6,    0,   84,    2, 0x0a /* Public */,
       7,    0,   85,    2, 0x0a /* Public */,
       8,    1,   86,    2, 0x0a /* Public */,
       9,    1,   89,    2, 0x0a /* Public */,
      10,    0,   92,    2, 0x0a /* Public */,
      11,    0,   93,    2, 0x0a /* Public */,
      12,    0,   94,    2, 0x0a /* Public */,
      13,    0,   95,    2, 0x0a /* Public */,
      14,    0,   96,    2, 0x0a /* Public */,
      15,    1,   97,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    2,
    QMetaType::Void, QMetaType::QModelIndex,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    2,

       0        // eod
};

void UserView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UserView *_t = static_cast<UserView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->supressed(); break;
        case 1: _t->updateView((*reinterpret_cast< QModelIndexList(*)>(_a[1]))); break;
        case 2: _t->updateInbox(); break;
        case 3: _t->updateFriends(); break;
        case 4: _t->updateSent(); break;
        case 5: _t->showPost((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 6: _t->showMyPost((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 7: _t->addRelation(); break;
        case 8: _t->newRelation(); break;
        case 9: _t->newPost(); break;
        case 10: _t->deleteUser(); break;
        case 11: _t->deleteRelation(); break;
        case 12: _t->enableDeleteRelation((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QModelIndexList >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (UserView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UserView::supressed)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject UserView::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_UserView.data,
      qt_meta_data_UserView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *UserView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UserView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_UserView.stringdata0))
        return static_cast<void*>(const_cast< UserView*>(this));
    return QWidget::qt_metacast(_clname);
}

int UserView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void UserView::supressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
